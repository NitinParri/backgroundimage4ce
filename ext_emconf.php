<?php
/***************************************************************
 * Extension Manager/Repository config file for ext "backgroundimage4ce".
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Background image for content elements',
  'description' => 'Allows background images and background options for all types of content elements (text, text with image, forms, plugins, ...). Background imagess are adaptive. Supports rendering of content elements with Fluid and css_styled_content.',
  'category' => 'fe',
  'version' => '4.1.3',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => NULL,
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '9.5.0-10.4.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
      'bootstrap_package' => '',
      'fluid_styled_content' => '',
      'gridelements' => '',
    ),
  ),
);