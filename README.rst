=============
Documentation
=============

----------------
What does it do?
----------------

This extension allows background images and background options for all content elements (text, text with image, forms, plugins, FCEs, ...).


--------
Features
--------

- Background image and background color for all types of content elements
- All available CSS settings for background images can be configured (color, repeat, position, size, attachment)
- Supports rendering of content elements with Fluid and css_styled_content
- Height of content element can be set to a fixed height
- Height of content element can automatically be set to the height of the background image
- Adaptive background image (for smaller screen devices smaller images are loaded) with media queries (not with EXT:css_styled_content)
- CSS styles are stored in a CSS file which is included in head (inline styles with EXT:css_styled_content)
- Easy handling for editors


-----------
Screenshots
-----------

.. figure:: Documentation/screenshot1.png

    The upload field for the background image and the configuration options.


-----------
HTML output
-----------

This extension provides a class (for Fluid Templates with a viewhelper) and generates CSS code for this class:

::

    <div id="c1854" class="frame backgroundimage-c1854 default">

::

    .backgroundimage-c1854 {
        background-color: #cc3366;
        background-repeat: repeat;
    }
    @media all {
        .backgroundimage-c1854 {
            background-image: url("/fileadmin/_processed_/csm_bg_download_7199d18411.jpg");
        }
    }
    @media all and (min-width: 768px) {
        .backgroundimage-c1854 {
            background-image: url("/fileadmin/_processed_/csm_bg_download_030f57882e.jpg");
        }
    }
    @media all and (min-width: 992px) {
        .backgroundimage-c1854 {
            background-image: url("/fileadmin/_processed_/csm_bg_download_05d9514221.jpg");
        }
    }
    @media all and (min-width: 1200px) {
        .backgroundimage-c1854 {
            background-image: url("/fileadmin/_processed_/csm_bg_download_05d9514221.jpg");
        }
    }


-------------
Configuration
-------------

After installing this extension via the extension manager and updating the database, you have to include the extension template in field "Include static (from extensions)" in your main template on **last** position.

Last position because it overwrites Fluid Templates and TypoScript settings which are sometimes overwritten by other extensions.

At least, you should add this template **after** the static templates of EXT:css_styled_content, bootstrap_package and fluid_styled_content.

Then, the fields for the background image and background options are available in tab "Appearance" of every content element.


Overview of TypoScript constants
================================

=======================  =========  ==========================  ===========
Property                 Data type  Default                     Description
=======================  =========  ==========================  ===========
padding                  integer    0                           This value is subtracted from the height of the content element. Could be useful if your content element with the background image has a padding and get's a fixed height. Not necessary with CSS rule "box-sizing: border-box".
adaptiveImages           boolean    1                           Generate optimized images for different screen sizes (mobile, tablet, desktop)
adaptiveImages.xs.image  integer    500                         Maximum image size for extra small devices (phones) in pixel
adaptiveImages.sm.image  integer    970                         Maximum image size for small devices (tablets) in pixel
adaptiveImages.md.image  integer    1170                        Maximum image size for medium devices (desktops) in pixel
adaptiveImages.lg.image  integer    1170                        Maximum image size for large devices (desktops) in pixel
adaptiveImages.xs.media  string     all                         Breakpoint of screen size for extra small devices (phones) in pixel
adaptiveImages.sm.media  string     all and (min-width: 768px)  Breakpoint of screen size for small devices (tablets) in pixel
adaptiveImages.md.media  string     all and (min-width: 992px)  Breakpoint of screen size for medium devices (desktops) in pixel
adaptiveImages.lg.media  string     all and (min-width: 1200px) Breakpoint of screen size for large devices (desktops) in pixel
image.max                integer    1200                        Reduce image size, if uploaded image is larger than this size (only if adaptive images are disabled)
=======================  =========  ==========================  ===========


Preview of settings
===================

To see the background options in the page module, use EXT:cefooter.


------------------------
Usage in Fluid Templates
------------------------

If you use this extension with EXT:bootstrap_package or fluid_styled_content, the CSS class is provided by a viewhelper:

::

    {namespace bg4ce=SBTheke\Backgroundimage4ce\ViewHelpers}
    [...]
    <div id="c{data.uid}" class="frame {bg4ce:renderBackground(cObj: data)}">

The Fluid templates of both extensions are modified and overwritten by this extension, so in most cases you don't have to do anything.
But if you or a third extension overwrite these templates, you have to include this viewhelper.


----------
To-Do list
----------

Preview of the background image in the page module. Sponsoring is welcome.

Write me if you have any suggestions or improvements.


---
FAQ
---

Usage with TYPO3 < 9
====================

Please use version 4.0.0 from TER.

Usage with TYPO3 < 8
====================

Please use version 3.0.0 from TER.


Usage with TYPO3 < 7
====================

Please use version 2.0.3 from TER.


Usage with TYPO3 < 6.2
======================

Please use version 1.2.2 from TER.


--------
Problems
--------

Usage with EXT:bootstrap_package: There are no fields for the background image in tab "Appearance"
===================================================================================================

The EXT:bootstrap_package configures all content elements from scratch.
Therefore, this extension has to be loaded AFTER EXT:bootstrap_package. To do that, move the entry of this extension in file PackageStates.php after EXT:bootstrap_package.


Usage with EXT:gridelements: There are no fields for the background image in tab "Appearance"
==============================================================================================

This extension has to be loaded AFTER EXT:gridelements. To do that, move the entry of this extension in file PackageStates.php after EXT:gridelements.


Usage with EXT:css_styled_content: There's no output in frontend
=================================================================

Have a look in your TypoScript settings. If you changed something in "tt_content.stdWrap.innerWrap", this is surely the reason.
Or perhaps some extension you use change these settings, e.g. extension "cewrap". Then it could help to include the static template of EXT:backgroundimage4ce before these one.


---------
ChangeLog
---------

See file **ChangeLog** in the extension directory.


----------
Sponsoring
----------

Many thanks to reinstil GmbH & Co KG (www.reinstil.de) for sponsoring the initial release of this extension.