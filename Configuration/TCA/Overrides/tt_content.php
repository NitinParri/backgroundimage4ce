<?php
defined('TYPO3_MODE') || die();

// Extra fields for the tt_content table
$newContentColumns = [
    'tx_backgroundimage4ce_active' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_active',
        'onChange' => 'reload',
        'config' => [
            'type' => 'check',
        ],
    ],
    'tx_backgroundimage4ce_image' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_image',
        'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'tx_backgroundimage4ce_image',
            [
                'appearance' => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                ],
                'maxitems' => 1,
                'foreign_match_fields' => [
                    'fieldname' => 'tx_backgroundimage4ce',
                ],
                'overrideChildTca' => [
                    'types' => [
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_UNKNOWN => [
                            'showitem' => '
                            --palette--;;filePalette
                        '
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;;filePalette
                        '
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            crop,
                            --palette--;;filePalette
                        '
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;;filePalette
                        '
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;;filePalette
                        '
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;;filePalette
                        '
                        ],
                    ],
                ],
            ],
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
        ),
    ],
    'tx_backgroundimage4ce_repeat' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_repeat',
        'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [
                    'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_default',
                    ''
                ],
                [
                    'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_repeat.repeat',
                    'repeat'
                ],
                [
                    'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_repeat.norepeat',
                    'no-repeat'
                ],
                [
                    'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_repeat.repeatx',
                    'repeat-x'
                ],
                [
                    'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_repeat.repeaty',
                    'repeat-y'
                ],
            ],
        ],
    ],
    'tx_backgroundimage4ce_color' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_color',
        'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
        'config' => [
            'type' => 'input',
            'max' => '20',
            'eval' => 'trim',
            'wizards' => [
                'colorChoice' => [
                    'type' => 'colorbox',
                    'module' => [
                        'name' => 'wizard_colorpicker',
                    ],
                    'JSopenParams' => 'height=500,width=500,status=0,menubar=0,scrollbars=1',
                ],
            ],
        ],
    ],
    'tx_backgroundimage4ce_position' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_position',
        'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
        'config' => [
            'type' => 'input',
            'max' => 20,
            'eval' => 'trim',
        ],
    ],
    'tx_backgroundimage4ce_size' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_size',
        'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
        'config' => [
            'type' => 'input',
            'max' => 20,
            'eval' => 'trim',
        ],
    ],
    'tx_backgroundimage4ce_attachment' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_attachment',
        'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [
                    'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_default',
                    ''
                ],
                [
                    'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_attachment.scroll',
                    'scroll'
                ],
                [
                    'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_attachment.fixed',
                    'fixed'
                ],
                [
                    'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_attachment.local',
                    'local'
                ],
            ],
        ],
    ],
    'tx_backgroundimage4ce_autoheight' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_autoheight',
        'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
        'config' => [
            'type' => 'check',
            'items' => [
                [
                    'LLL:EXT:lang/locallang_core.xlf:labels.enabled',
                    ''
                ]
            ],
        ],
    ],
    'tx_backgroundimage4ce_height' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.tx_backgroundimage4ce_height',
        'displayCond' => ['AND' => ['FIELD:tx_backgroundimage4ce_active:REQ:true', 'FIELD:tx_backgroundimage4ce_autoheight:=:0']],
        'config' => [
            'type' => 'input',
            'size' => 5,
            'max' => 5,
            'eval' => 'int',
        ],
    ],
];

// Adding fields to the tt_content table definition in TCA
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $newContentColumns);

// Create palette
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', '--palette--;LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.palette.backgroundimage4ce;backgroundimage4ce', '', 'after:layout');
$GLOBALS['TCA']['tt_content']['palettes']['backgroundimage4ce']['showitem'] = 'tx_backgroundimage4ce_active, --linebreak--, tx_backgroundimage4ce_image, --linebreak--, tx_backgroundimage4ce_repeat, tx_backgroundimage4ce_color, tx_backgroundimage4ce_position, tx_backgroundimage4ce_size, --linebreak--, tx_backgroundimage4ce_attachment, tx_backgroundimage4ce_autoheight, tx_backgroundimage4ce_height';

// Request an update
$GLOBALS['TCA']['tt_content']['ctrl']['requestUpdate'] .= (empty($GLOBALS['TCA']['tt_content']['ctrl']['requestUpdate']) === TRUE ? '' : ',') . 'tx_backgroundimage4ce_active, tx_backgroundimage4ce_autoheight';

// Use crop variants @TODO
#$GLOBALS['TCA']['tt_content']['columns']['tx_backgroundimage4ce_image']['config']['overrideChildTca']['columns']['crop'] = $GLOBALS['TCA']['tt_content']['types']['image']['columnsOverrides']['image']['config']['overrideChildTca']['columns']['crop'];